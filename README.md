##Índice

*[Descripción del proyecto](#descripción-del-proyecto)

*[Características de la aplicación y demostración](#características-de-la-aplicación-y-demostración)

*[Ejemplos de uso](#ejemplos-de-uso)


## Descripción del proyecto
Este proyecto se basa en un microservicio de ejemplo donde se expone un endpoint y se permite hacer un filtrado para recuperar los datos de una base de datos H2 integrada.

Se trata de un Microservicio "demo" generado a partir del framework Spring Boot y utilizando como gestor de dependencias maven.

El microservicio "demo" expone un controlador y un endpoint básico para procesar peticiones de tipo GET que devuelve un listado en formato JSON correspondiente a los datos existentes en memoria en H2.

El microservicio se inicializa con unos datos en memoria H2 que se pueden ver en el fichero resources/data.sql, así mismo se crea una tabla según el fichero resources/schema.sql

Las columnas de la base de datos H2 corresponden a esta definición:
-BRAND_ID: foreign key de la cadena del grupo (1 = ZARA).
-START_DATE , END_DATE: rango de fechas en el que aplica el precio tarifa indicado.
-PRICE_LIST: Identificador de la tarifa de precios aplicable.
-PRODUCT_ID: Identificador código de producto.
-PRIORITY: Desambiguador de aplicación de precios. Si dos tarifas coinciden en un rago de fechas se aplica la de mayor prioridad (mayor valor numérico).
-PRICE: precio final de venta.
-CURR: iso de la moneda.

## Características de la aplicación y demostración

- `Funcionalidad 1`: La única funcionalidad de este microservicio es un endpoint que acepta como parámetros de entrada: fecha de aplicación, identificador de producto, identificador de cadena.
  DevuelvE como datos de salida: identificador de producto, identificador de cadena, tarifa a aplicar, fechas de aplicación y precio final a aplicar

## Ejemplos de uso
Para levantar la aplicación se debe ejecutar esta sentencia en la ruta del proyecto:
mvn spring-boot:run

A continuación se listan unos curl de ejemplo de uso para la aplicación.
 1. curl --location --request GET 'http://localhost:8081/product/price?price_date=2020-06-14T10:00:00&product_id=35455&brand_id=1'
 2. curl --location --request GET 'http://localhost:8081/product/price?price_date=2020-06-14T16:00:00&product_id=35455&brand_id=1'
 3. curl --location --request GET 'http://localhost:8081/product/price?price_date=2020-06-14T21:00:00&product_id=35455&brand_id=1'
 4. curl --location --request GET 'http://localhost:8081/product/price?price_date=2020-06-15T10:00:00&product_id=35455&brand_id=1'
 5. curl --location --request GET 'http://localhost:8081/product/price?price_date=2020-06-16T21:00:00&product_id=35455&brand_id=1'

La respuesta obtenida por esta operación será de este tipo:
{
    "priceList": "1",
    "brandId": "1",
    "startDate": "2020-06-14T00:00:00",
    "endDate": "2020-12-31T23:59:59",
    "productId": "35455",
    "price": 35.50,
    "currency": "EUR"
}
