drop  table if exists PRICE;
create table PRICE (
    BRAND_ID varchar(50) NOT NULL,
    START_DATE TIMESTAMP  NOT NULL,
    END_DATE TIMESTAMP  NOT NULL,
    PRICE_LIST varchar(50) NOT NULL ,
    PRODUCT_ID varchar(50) NOT NULL,
    PRIORITY varchar(50) NOT NULL,
    PRICE numeric NOT NULL,
    CURR varchar(50) NOT NULL,
    PRIMARY KEY(PRICE_LIST)
);