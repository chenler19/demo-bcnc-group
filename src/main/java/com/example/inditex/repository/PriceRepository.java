package com.example.inditex.repository;

import com.example.inditex.entity.PricesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface PriceRepository extends JpaRepository<PricesEntity, String> {

    //H2 MAX function is not working as MAX function in ORACLE SQL
    @Query(value = "select TOP 1 p.PRICE_LIST, p.BRAND_ID, p.START_DATE, p.END_DATE, p.PRODUCT_ID, " +
                " MAX(PRIORITY) AS PRIORITY, p.PRICE, p.CURR " +
            " FROM PRICE p " +
            " WHERE p.START_DATE <= ?1 " +
            " AND p.END_DATE >= ?1 " +
            " AND p.PRODUCT_ID = ?2 " +
            " AND p.BRAND_ID = ?3 " +
            " GROUP BY p.PRICE_LIST, P.START_DATE " +
            " ORDER BY p.PRIORITY DESC ", nativeQuery = true)
    PricesEntity findByDateProductIdAndBranId(
            LocalDateTime operationDate,
            String productId,
            String brandId);

}
