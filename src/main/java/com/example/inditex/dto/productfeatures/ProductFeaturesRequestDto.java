package com.example.inditex.dto.productfeatures;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Product Features Request data
 */
@Getter
@Setter
@Builder
public class ProductFeaturesRequestDto {
    /** Price Date */
    private LocalDateTime priceDate;
    /** Product Id */
    private String productId;
    /** Brand Id */
    private String brandId;
}
