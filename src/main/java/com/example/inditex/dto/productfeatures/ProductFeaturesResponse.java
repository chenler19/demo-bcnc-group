package com.example.inditex.dto.productfeatures;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Product features class
 */
@Getter
@Setter
public class ProductFeaturesResponse {
    /** priceList */
    private String priceList;
    /** brandId */
    private String brandId;
    /** startDate */
    private LocalDateTime startDate;
    /** endDate */
    private LocalDateTime endDate;
    /** productId */
    private String productId;
//    /** priority */
//    private String priority;
    /** price */
    private BigDecimal price;
    /** currency */
    private String currency;
}
