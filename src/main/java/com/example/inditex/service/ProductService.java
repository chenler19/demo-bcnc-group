package com.example.inditex.service;

import com.example.inditex.dto.productfeatures.ProductFeaturesResponse;
import com.example.inditex.dto.productfeatures.ProductFeaturesRequestDto;
import com.example.inditex.entity.PricesEntity;
import com.example.inditex.repository.PriceRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

/**
 * Product Service class
 */
@Slf4j
@Service
public class ProductService {

    /** PriceRepository */
    private final PriceRepository priceRepository;

    /** Model mapper */
    private final ModelMapper modelMapper;

    /**
     * Constructor
     * @param priceRepository   price repository
     * @param modelMapper       model mapper
     */
    public ProductService(PriceRepository priceRepository, ModelMapper modelMapper) {
        this.priceRepository = priceRepository;
        this.modelMapper = modelMapper;
    }

    /**
     * Get products features from PRICE table
     * @param requestDto    request dto
     * @return list of objects from Price table
     */
    public ProductFeaturesResponse getProductFeatures(ProductFeaturesRequestDto requestDto) throws Exception {
        try {
            // Find in price table by priceDate, productId and brandId.
            PricesEntity pricesFeatures = priceRepository.findByDateProductIdAndBranId(
                    requestDto.getPriceDate(),
                    requestDto.getProductId(),
                    requestDto.getBrandId());

            log.debug("Row selected in getProductFeatures: {}",pricesFeatures);

            // Entity to DTO
            if(pricesFeatures==null) {
                return null;
            } else {
                return this.modelMapper.map(pricesFeatures, ProductFeaturesResponse.class);
            }

        } catch (Exception e) {
            String msg = String.format("Error at getProductFeatures method, error: %s", e.getMessage());
            log.error(msg,e);
            throw new Exception(e);
        }
    }
}
