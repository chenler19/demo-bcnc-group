package com.example.inditex.entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name="PRICE")
public class PricesEntity implements Serializable {

    /** Serializable */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="PRICE_LIST")
    private String priceList;

    @Column(name="BRAND_ID")
    private String brandId;

    @Column(name="START_DATE")
    private LocalDateTime startDate;

    @Column(name="END_DATE")
    private LocalDateTime endDate;

    @Column(name="PRODUCT_ID")
    private String productId;

    @Column(name="PRIORITY")
    private String priority;

    @Column(name="PRICE")
    private BigDecimal price;

    @Column(name="CURR")
    private String currency;

}
