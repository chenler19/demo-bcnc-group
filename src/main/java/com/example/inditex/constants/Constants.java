package com.example.inditex.constants;

/**
 * Constants class
 */
public class Constants {
    /** Msg internal server error */
    public static String MSG_INTERNAL_SERVER_ERROR = "Este servicio se encuentra temporalmente inactivo, " +
            "vuelva a probarlo más tarde.";
}
