package com.example.inditex.controller;

import com.example.inditex.constants.Constants;
import com.example.inditex.dto.productfeatures.ProductFeaturesResponse;
import com.example.inditex.dto.productfeatures.ProductFeaturesRequestDto;
import com.example.inditex.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * Controller Demo
 */
@Slf4j
@RequestMapping("/product")
@RestController
public class DemoController {

    /** product service */
    private final ProductService productService;

    /**
     * Constructor
     * @param productService    product service
     */
    public DemoController(ProductService productService) {
        this.productService =  productService;
    }

    /**
     * End point product_features
     * @param priceDate Price Date
     * @param productId Product Identificador
     * @param brandId   Brand identificador
     * @return response entity
     */
    @GetMapping("/price")
    public ResponseEntity<Object> getProductFeatures(
            @RequestParam(name = "price_date", required = true)
                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime priceDate,
            @RequestParam(name = "product_id", required = true) String productId,
            @RequestParam(name = "brand_id", required = true) String brandId
    ) {
        // Build request input data
        ProductFeaturesRequestDto requestDto = ProductFeaturesRequestDto.builder()
                .priceDate(priceDate)
                .productId(productId)
                .brandId(brandId)
                .build();
        log.info("Request Data input: {}", requestDto);

        return getResponseEntityProductFeatures(requestDto);
    }

    /**
     * Private method to get response entity product features
     * @param requestDto  request data input
     * @return  Response entity product features
     */
    private ResponseEntity<Object> getResponseEntityProductFeatures(ProductFeaturesRequestDto requestDto) {
        long start = System.currentTimeMillis();
        log.debug("Start getResponseEntityProductFeatures method.");

        try {
            // Product service getting features
            ProductFeaturesResponse response = productService.getProductFeatures(requestDto);

            // if response empty then 204 No Content, else 200
            if (response==null) {
                log.debug("Products features are empty or null.");
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            } else {
                log.debug("Response 200");
                return new ResponseEntity<>(response,HttpStatus.OK);
            }
        } catch (Exception e) {
            // If exception then response with code 500 and message predefined
            log.error(e.getMessage());
            Exception ex = new Exception(Constants.MSG_INTERNAL_SERVER_ERROR, e);
            return new ResponseEntity<>(ex, HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            log.info("End Get product_features. Total execution time: {} ms.", System.currentTimeMillis() - start);
        }
    }
}
