package com.example.inditex.config;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {


	/**
	 * Constructor without parameters. In this method type map of different DTO's
	 * classes are defined.
	 *
	 * @return ModelMapper.
	 */
	@Bean
	public ModelMapper modelMapper() {

		ModelMapper modelMapper = new ModelMapper();
		// Field matching enabled.
		modelMapper.getConfiguration().setFieldMatchingEnabled(true);
		// Private access level.
		modelMapper.getConfiguration().setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE);
		// Standard matching strategies.
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STANDARD);
		// Ignored ambiguity.
		modelMapper.getConfiguration().setAmbiguityIgnored(true);
		// Skipped nulls.
		modelMapper.getConfiguration().setSkipNullEnabled(true);

		return modelMapper;
	}
}