package com.example.inditex.controller;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class DemoControllerTest {

    private String URL_TEMPLATE = "/product/price";

    private String PRICE_DATE = "price_date";
    private String PRODUCT_ID = "product_id";
    private String BRAND_ID = "brand_id";

    private String JSON_PATH_PRICE = "$.price";
    private String JSON_PATH_PRODUCT_ID = "$.productId";
    private String JSON_PATH_BRAND_ID = "$.brandId";

    @Autowired
    private MockMvc mockMvc;

    @Test
    @SqlGroup({
            @Sql(value = "classpath:/schema.sql", executionPhase = BEFORE_TEST_METHOD),
            @Sql(value = "classpath:/data.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    void getProductFeaturesTest1() throws Exception {
        String priceDate = "2020-06-14T10:00:00";
        String productId = "35455";
        String brandId = "1";

        this.mockMvc.perform(get(URL_TEMPLATE)
                .param(PRICE_DATE, priceDate)
                .param(PRODUCT_ID, productId)
                .param(BRAND_ID,brandId))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath(JSON_PATH_PRICE).value(35.50))
            .andExpect(jsonPath(JSON_PATH_PRODUCT_ID).value(productId))
            .andExpect(jsonPath(JSON_PATH_BRAND_ID).value(brandId));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:/schema.sql", executionPhase = BEFORE_TEST_METHOD),
            @Sql(value = "classpath:/data.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    void getProductFeaturesTest2() throws Exception {
        String priceDate = "2020-06-14T16:00:00";
        String productId = "35455";
        String brandId = "1";

        this.mockMvc.perform(get(URL_TEMPLATE)
                    .param(PRICE_DATE, priceDate)
                    .param(PRODUCT_ID, productId)
                    .param(BRAND_ID,brandId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_PATH_PRICE).value(25.45))
                .andExpect(jsonPath(JSON_PATH_PRODUCT_ID).value(productId))
                .andExpect(jsonPath(JSON_PATH_BRAND_ID).value(brandId));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:/schema.sql", executionPhase = BEFORE_TEST_METHOD),
            @Sql(value = "classpath:/data.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    void getProductFeaturesTest3() throws Exception {
        String priceDate = "2020-06-14T21:00:00";
        String productId = "35455";
        String brandId = "1";

        this.mockMvc.perform(get(URL_TEMPLATE)
                .param(PRICE_DATE, priceDate)
                .param(PRODUCT_ID, productId)
                .param(BRAND_ID,brandId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_PATH_PRICE).value(35.50))
                .andExpect(jsonPath(JSON_PATH_PRODUCT_ID).value(productId))
                .andExpect(jsonPath(JSON_PATH_BRAND_ID).value(brandId));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:/schema.sql", executionPhase = BEFORE_TEST_METHOD),
            @Sql(value = "classpath:/data.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    void getProductFeaturesTest4() throws Exception {
        String priceDate = "2020-06-15T10:00:00";
        String productId = "35455";
        String brandId = "1";

        this.mockMvc.perform(get(URL_TEMPLATE)
                .param(PRICE_DATE, priceDate)
                .param(PRODUCT_ID, productId)
                .param(BRAND_ID,brandId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_PATH_PRICE).value(30.50))
                .andExpect(jsonPath(JSON_PATH_PRODUCT_ID).value(productId))
                .andExpect(jsonPath(JSON_PATH_BRAND_ID).value(brandId));
    }

    @Test
    @SqlGroup({
            @Sql(value = "classpath:/schema.sql", executionPhase = BEFORE_TEST_METHOD),
            @Sql(value = "classpath:/data.sql", executionPhase = BEFORE_TEST_METHOD)
    })
    void getProductFeaturesTest5() throws Exception {
        String priceDate = "2020-06-16T21:00:00";
        String productId = "35455";
        String brandId = "1";

        this.mockMvc.perform(get(URL_TEMPLATE)
                .param(PRICE_DATE, priceDate)
                .param(PRODUCT_ID, productId)
                .param(BRAND_ID,brandId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath(JSON_PATH_PRICE).value(38.95))
                .andExpect(jsonPath(JSON_PATH_PRODUCT_ID).value(productId))
                .andExpect(jsonPath(JSON_PATH_BRAND_ID).value(brandId));
    }
}
